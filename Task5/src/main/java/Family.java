import java.util.Arrays;

public  class Family extends Human{
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father, Human[] children, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }
    public void addChild(Human human){
        Human[] child = new Human[children.length+1];
        for (int i = 0; i <children.length ; i++) {
            child[i]=children[i];
        }
        child[children.length]=human;
        children=child;
    }
    public boolean deleteChild(int index){
        if (index<0 || index>= children.length){
            return false;
        }
        boolean a=false;

        Human[] removeChild = new Human[children.length-1];
        for (int i = 0,k=0; i <children.length ; i++) {
            if (i == index){
                a=true;
            continue;
            }
            removeChild[k++]=children[i];
        }
    children=removeChild;
    return a;
    }
    public int countFamily(){
        if (mother==null && father==null){
            return children.length;
        }
        else if (mother==null){
            return 1+children.length;
        }
        else if (father==null){
            return 1+children.length;
        }
        return 2+children.length;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
    }
}
