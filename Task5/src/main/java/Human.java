import java.util.Arrays;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int IQ;
    private String[][] schedule;
    private Family family;

    public Human(){}

    public Human(String[][] schedule) {
        this.schedule = schedule;
    }

    public Human(String name,String surname, int year, int IQ,String[][] schedule) {
        this.surname = surname;
        this.year = year;
        this.IQ = IQ;
        this.name = name;
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIQ() {
        return IQ;
    }

    public void setIQ(int IQ) {
        this.IQ = IQ;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return getIQ() == human.getIQ();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIQ());
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", IQ=" + IQ +
                ", Schedule=" + Arrays.deepToString(schedule) +
                '}';
    }
}
